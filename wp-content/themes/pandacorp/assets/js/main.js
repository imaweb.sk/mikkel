/**
 * Main JavaScript file.
 */
window.addEventListener('DOMContentLoaded', function() {
    var menuOpener = document.getElementById('menu-opener');
    var submenu = document.getElementById('submenu');
    menuOpener.addEventListener("click", function () {
        submenu.classList.toggle("opened")
    });
});
