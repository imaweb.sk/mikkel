<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Rommel
 * @subpackage PandaCorp
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="profile" href="https://gmpg.org/xfn/11"/>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<?php wp_body_open(); ?>

	<div id="site-header">
		<?php //@TODO: Setup the header. ?>
        <div class="logo-area">
            <a href="/">
                <?php if (has_custom_logo()) {
                    echo '<img src="' . esc_url(wp_get_attachment_image_src(get_theme_mod('custom_logo') , 'full')[0]) . '" alt="' . get_bloginfo( 'name' ) . '">';
                } else {
                    echo '<h1>' . get_bloginfo('name') . '</h1>';
                } ?>
            </a>
        </div>
        <div class="navigation-area">
            <?php
                wp_nav_menu( array(
                    'theme_location' => 'primary'
                ));
            ?>
            <svg width="24" height="16" viewBox="0 0 24 16" fill="none" xmlns="http://www.w3.org/2000/svg" id="menu-opener">
                <rect width="24" height="1" />
                <rect y="5" width="24" height="1" />
                <rect y="10" width="24" height="1" />
                <rect y="15" width="12" height="1" />
            </svg>
        </div>
        <div class="submenu" id="submenu">
            <?php
                wp_nav_menu( array(
                    'theme_location' => 'secondary'
                ));
            ?>
        </div>



	</div>



	<div id="content" class="site-content">
