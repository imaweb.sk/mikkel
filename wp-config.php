<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'panda' );

/** MySQL database username */
define( 'DB_USER', 'panda' );

/** MySQL database password */
define( 'DB_PASSWORD', 'panda' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '/Cx,t~k<eq1 H#<T.T,[H{3Ha?9RPUu5_YU5xZS:dRH]f+9[.-?JLJ=H<]G9e-Q@' );
define( 'SECURE_AUTH_KEY',  'Y.ZfyMa8;BeXRef?#-$Vix2Oj33Q@Xx:n:`lDra1Xbm [UJT~|^Iu_|Aq@F|f,jF' );
define( 'LOGGED_IN_KEY',    'h2VElFy^H5OyTBD?-Z`Gcxu(~WrE_56lsl5_r6}rbz<}@d;6c>j2<>Q$iM&s!-`,' );
define( 'NONCE_KEY',        '<2}e&,e%%UBp}A}UoTBt6;Sr1xAv4|aYr6[M%U*@apmv?n$7o:20P6(D8tmH~{t`' );
define( 'AUTH_SALT',        '#IiY2Pc]eUHd?~W1x*{l|S6;K [L$&>j(%pP>}n4Gr}|YrMOV/c(=WipT}x}aeL?' );
define( 'SECURE_AUTH_SALT', 'VEghgNq*aio$*uPzab>vB o!e[lSKp9r;uJtyJP)FZX=GoRLOq<RrnGN<_v,&JM&' );
define( 'LOGGED_IN_SALT',   'j<>3|/yX!L;MK&0s6Ar-;uD}0bZaOWdmp4:6--M<G{k0i3Cz*D:&QN;@._Z:crp]' );
define( 'NONCE_SALT',       'UK#/%2Q4?:K^)Qb+S0ED5Ycx_6WBA_4.%L:eBjzONYT3ZHI2T3/@76w;>=.s[.*4' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'panda_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
